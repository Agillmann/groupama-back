'use strict';

/**
 * Formulaire.js controller
 *
 * @description: A set of functions called "actions" for managing `Formulaire`.
 */

module.exports = {

  /**
   * Retrieve formulaire records.
   *
   * @return {Object|Array}
   */

  find: async (ctx, next, { populate } = {}) => {
    if (ctx.query._q) {
      return strapi.services.formulaire.search(ctx.query);
    } else {
      return strapi.services.formulaire.fetchAll(ctx.query, populate);
    }
  },

  /**
   * Retrieve a formulaire record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.formulaire.fetch(ctx.params);
  },

  /**
   * Count formulaire records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.formulaire.count(ctx.query);
  },

  /**
   * Create a/an formulaire record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.formulaire.add(ctx.request.body);
  },

  /**
   * Update a/an formulaire record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.formulaire.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an formulaire record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.formulaire.remove(ctx.params);
  }
};
